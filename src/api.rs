use axum::{
	extract::{Path, State},
	routing::{get, post},
	Json, Router,
};
use serde::{Deserialize, Serialize};
use url::Url;

use crate::{
	error::{ServerError, UwurlError},
	SharedAppState,
};

pub(crate) fn routes() -> Router<SharedAppState> {
	Router::new().route("/insert_url", post(insert_url)).route("/get_url/:short", get(get_url))
}

#[derive(Serialize, Deserialize, Debug)]
struct ShortenedUrl {
	shortened: Url,
}

#[derive(Serialize, Deserialize, Debug)]
struct UrlPayload {
	url: Url,
}

async fn insert_url(
	State(state): State<SharedAppState>,
	Json(request): Json<UrlPayload>,
) -> Result<Json<ShortenedUrl>, UwurlError> {
	let shortened = crate::url::shorten(&state, state.host.clone(), request.url)
		.await
		.map_err(ServerError::from)?;
	Ok(Json::from(ShortenedUrl { shortened }))
}

async fn get_url(
	State(state): State<SharedAppState>,
	Path(short): Path<String>,
) -> Result<Json<UrlPayload>, UwurlError> {
	let url = crate::url::get_url(&state, short).await?;
	Ok(Json::from(UrlPayload { url }))
}
