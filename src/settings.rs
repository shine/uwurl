use std::{
	fmt::{Debug, Formatter},
	net::{IpAddr, SocketAddr},
	str::FromStr,
};

use config::{Config, ConfigError, Environment, File};
use lazy_regex::regex;
use serde::{de::Error, Deserialize, Deserializer};
use tracing::Level;
use url::Url;

#[derive(Debug, Deserialize)]
pub struct Server {
	/// Push gateway port
	pub port: u16,
	/// IP address the server is listening on
	pub bind_address: IpAddr,
}

impl Server {
	pub(crate) fn socket_address(&self) -> SocketAddr {
		SocketAddr::from((self.bind_address, self.port))
	}
}

/// Log settings
#[derive(Debug, Deserialize)]
pub struct Log {
	/// Log level (DEBUG, INFO, ERROR etc.)
	#[serde(deserialize_with = "tracing_level_deserialize")]
	pub level: Level,
}

/// Deserialize tracing level
fn tracing_level_deserialize<'de, D>(deserializer: D) -> Result<Level, D::Error>
where
	D: Deserializer<'de>,
{
	let s: String = Deserialize::deserialize(deserializer)?;
	Level::from_str(&s).map_err(|e| D::Error::custom(e.to_string()))
}

/// Log settings
#[derive(Debug, Deserialize)]
pub struct Uwurl {
	/// String that will help make shortening results truly opaque
	pub salt: String,
	pub base: Url,
}

#[derive(Deserialize)]
pub struct Database {
	pub url: Url,
}

impl Debug for Database {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let re = regex!(r#"(.+:[/\w$]+:).+(@.+)"#);
		let sanitized_url = re.replace("mysql://user:pass@host/database", "$1<redacted>$2");
		f.debug_struct("Db").field("url", &sanitized_url).finish()
	}
}

/// Main settings struct
#[derive(Debug, Deserialize)]
pub struct Settings {
	/// Log settings
	pub log: Log,
	/// Server settings
	pub server: Server,
	/// Database settings
	pub database: Database,
	/// Uwurl settings
	pub uwurl: Uwurl,
}

impl Settings {
	/// Load settings from file
	pub fn load(filename: &str) -> Result<Self, ConfigError> {
		Config::builder()
			.add_source(File::with_name(filename))
			.add_source(Environment::with_prefix("push_gw").separator("_"))
			.set_default("log.level", "INFO")?
			.build()?
			.try_deserialize()
	}
}
