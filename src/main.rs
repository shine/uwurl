use miette::{IntoDiagnostic, Report};
use tracing::info;
use tracing_subscriber::FmtSubscriber;
use uwurl::initialize;

#[tokio::main]
async fn main() -> Result<(), Report> {
	let settings = uwurl::settings::Settings::load("config.yaml").into_diagnostic()?;
	FmtSubscriber::builder().with_max_level(settings.log.level).init();
	info!("Logging initialized, starting app");
	initialize(settings).await
}
