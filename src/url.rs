use sea_orm::{ActiveModelTrait, ActiveValue, EntityTrait};
use url::Url;

use crate::{
	entities::{link, link::Entity as Link},
	error::{HttpError, ServerError, UwurlError},
	SharedAppState,
};

pub(crate) async fn shorten(
	state: &SharedAppState,
	host: Url,
	url: Url,
) -> Result<Url, ServerError> {
	let link =
		link::ActiveModel { id: ActiveValue::NotSet, url: ActiveValue::Set(url.to_string()) };
	let link = link.insert(&state.db).await?;
	let short_url = host.join(&state.harsh.encode(&[link.id.into()]))?;
	Ok(short_url)
}

pub(crate) async fn get_url(state: &SharedAppState, short: String) -> Result<Url, UwurlError> {
	let id = state
		.harsh
		.decode(short)
		.map_err(ServerError::from)?
		.get(0)
		.copied()
		.ok_or(HttpError::NotFound)?;
	let url = Link::find_by_id(u32::try_from(id).map_err(ServerError::from)?)
		.one(&state.db)
		.await
		.map_err(ServerError::from)?
		.ok_or(HttpError::NotFound)?
		.url
		.as_str()
		.try_into()
		.map_err(ServerError::from)?;
	Ok(url)
}
