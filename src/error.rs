use std::num::TryFromIntError;

use axum::{
	http::{header::InvalidHeaderValue, uri::InvalidUriParts, StatusCode},
	response::{IntoResponse, Response},
};
use sea_orm::DbErr;
use thiserror::Error;

#[derive(Error, Debug)]
#[error(transparent)]
pub enum ServerError {
	Database(#[from] DbErr),
	Harsh(#[from] harsh::Error),
	IntError(#[from] TryFromIntError),
	UrlParse(#[from] url::ParseError),
	InvalidUriParts(#[from] InvalidUriParts),
	IvalidHeaderValue(#[from] InvalidHeaderValue),
	AxumHeaders(#[from] axum::headers::Error),
}

impl IntoResponse for ServerError {
	fn into_response(self) -> Response {
		(StatusCode::INTERNAL_SERVER_ERROR, self.to_string()).into_response()
	}
}

#[derive(Error, Debug)]
pub enum HttpError {
	#[error("Link not found")]
	NotFound,
}

impl IntoResponse for HttpError {
	fn into_response(self) -> Response {
		let err_code = match self {
			Self::NotFound => StatusCode::NOT_FOUND,
		};
		(err_code, self.to_string()).into_response()
	}
}

#[derive(Error, Debug)]
#[error(transparent)]
pub enum UwurlError {
	Server(#[from] ServerError),
	Http(#[from] HttpError),
}

impl IntoResponse for UwurlError {
	fn into_response(self) -> Response {
		match self {
			Self::Http(err) => err.into_response(),
			Self::Server(err) => err.into_response(),
		}
	}
}
