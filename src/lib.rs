mod api;
mod entities;
mod error;
pub mod settings;
mod url;
mod web;

use std::sync::Arc;

use ::url::Url;
use axum::{routing::get, Router};
use harsh::Harsh;
use miette::{IntoDiagnostic, Result};
use migration::{Migrator, MigratorTrait};
use sea_orm::{Database, DatabaseConnection};
use tracing::info;

use crate::settings::Settings;

pub(crate) struct AppState {
	pub(crate) harsh: Harsh,
	pub(crate) db: DatabaseConnection,
	pub(crate) style: String,
	pub(crate) host: Url,
}

pub(crate) type SharedAppState = Arc<AppState>;

pub async fn initialize(settings: Settings) -> Result<()> {
	info!("Initializing Harsh ID hasher");
	let harsh =
		Harsh::builder().salt(settings.uwurl.salt.clone()).length(6).build().into_diagnostic()?;
	let db = database(&settings.database.url).await.into_diagnostic()?;
	let style = grass::from_path("sass/basic.scss", &Default::default()).into_diagnostic()?;
	let shared_app_state = Arc::new(AppState { harsh, db, style, host: settings.uwurl.base });
	let app = Router::new()
		.route("/health", get(|| async { "Works!" }))
		.nest("/api", api::routes())
		.nest("/", web::routes())
		.with_state(shared_app_state);
	info!("Initializing app server on address: http://{:#?}/", settings.server.socket_address());
	axum::Server::bind(&settings.server.socket_address())
		.serve(app.into_make_service())
		.await
		.into_diagnostic()?;
	Ok(())
}

async fn database(url: &Url) -> Result<DatabaseConnection, sea_orm::DbErr> {
	let connection = Database::connect(url.to_string()).await?;
	Migrator::refresh(&connection).await?;
	Ok(connection)
}
