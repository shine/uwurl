use askama::Template;
use axum::{
	extract::{Path, State},
	headers::{ContentType, Header, HeaderMapExt, Location},
	response::{IntoResponse, Response},
	routing::{get, post},
	Form, Router,
};
use hyper::{header::HeaderValue, HeaderMap, StatusCode};
use serde::Deserialize;
use url::Url;

use crate::{
	error::{ServerError, UwurlError},
	SharedAppState,
};

pub(crate) fn routes() -> Router<SharedAppState> {
	Router::new()
		.route("/", get(index))
		.route("/shorten", post(shorten))
		.route("/:short", get(redirect_to_url))
		.route("/style.css", get(style))
}

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {
	title: String,
}

#[derive(Template)]
#[template(path = "shorten.html")]
struct ShortenTemplate {
	title: String,
	short_url: Url,
}

#[derive(Deserialize)]
struct ShortenRequestForm {
	url: Url,
}

async fn index() -> IndexTemplate {
	IndexTemplate { title: String::from("blep") }
}

async fn shorten(
	State(state): State<SharedAppState>,
	Form(ShortenRequestForm { url }): Form<ShortenRequestForm>,
) -> Result<ShortenTemplate, ServerError> {
	let short_url = crate::url::shorten(&state, state.host.clone(), url).await?;
	Ok(ShortenTemplate { title: String::from("blep"), short_url })
}

async fn redirect_to_url(
	State(state): State<SharedAppState>,
	Path(short): Path<String>,
) -> Result<Response, UwurlError> {
	let url = crate::url::get_url(&state, short).await?;
	let header_value = HeaderValue::from_str(&url.to_string()).map_err(ServerError::from)?;
	let mut headers = HeaderMap::default();
	headers.typed_insert(Location::decode(&mut [header_value].iter()).map_err(ServerError::from)?);
	let mut response = (StatusCode::PERMANENT_REDIRECT, ()).into_response();
	response.headers_mut().extend(headers);
	Ok(response)
}

async fn style(State(state): State<SharedAppState>) -> Response {
	let mut headers = HeaderMap::default();
	headers.typed_insert(ContentType::from(mime::TEXT_CSS));
	let mut response = (StatusCode::OK, state.style.clone()).into_response();
	response.headers_mut().extend(headers);
	response
}
